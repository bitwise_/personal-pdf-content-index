import json
import re
import sys
import spacy
from spacy.tokenizer import Tokenizer
from spacy.lang.it.stop_words import STOP_WORDS
from spacy.lang.it import Italian
from os import path


def set_custom_tokenizer(nlp_istance):
    special_cases = {":)": [{"ORTH": ":)"}]}
    prefix_re = re.compile(r'''^[[("']''')
    suffix_re = re.compile(r'''[])"']$''')
    infix_re = re.compile(r'''[-~'’]''')
    simple_url_re = re.compile(r'''^https?://''')

    return Tokenizer(nlp_istance.vocab, rules=special_cases,
                     prefix_search=prefix_re.search,
                     suffix_search=suffix_re.search,
                     infix_finditer=infix_re.finditer,
                     token_match=simple_url_re.match)


nlp = spacy.load("it_core_news_sm")
nlp.tokenizer = set_custom_tokenizer(nlp)


class TextAnalyzer:
    vocabulary = [line.rstrip() for line in
                  open(path.join(path.dirname(__file__), './datasets/vocabolario.txt'), 'r').readlines()]
    blacklist = [line.rstrip() for line in
                 open(path.join(path.dirname(__file__), './datasets/excluded_words.txt'), 'r').readlines()]

    def __init__(self, raw=None, include_discarded=False):
        if raw is None:
            raw = []
        self.raw_json = raw
        self.docs = []
        if include_discarded:
            self.docs = [nlp(self.raw_json[page_i]['text'] + self.raw_json[page_i]['discarded_text']) for page_i in range(0, len(self.raw_json))]
        else:
            self.docs = [nlp(self.raw_json[page_i]['text']) for page_i in range(0, len(self.raw_json))]
        self.collected_tokens = []
        self.cleaned_tokens = []

        self.collect_tokens()
        self.clean_tokens()

    def collect_tokens(self):
        for doc_i, doc in enumerate(self.docs):
            self.collected_tokens.append([t.text for t in doc if t.text.lower() not in STOP_WORDS])

    def clean_tokens(self):
        for pt_i, pt in enumerate(self.collected_tokens):
            self.cleaned_tokens.append([t for t in pt if self.accept_token(t)])

    def accept_token(self, t_token):
        t_token = t_token.lower()
        in_vocabulary = t_token in self.vocabulary
        in_blacklist = t_token in self.blacklist
        return in_vocabulary and not in_blacklist

    def preapre_output(self, which='clean'):
        return [{'page': pn, 'tokens': tkns} for pn, tkns in enumerate(self.cleaned_tokens)]

    def write(self, fp=sys.stdout):
        output = self.preapre_output()
        fp.write('[')
        for pg_i, tknzd_page in enumerate(output):
            json.dump(tknzd_page, fp, ensure_ascii=False)
            if pg_i < len(output) - 1:
                fp.write(',')
        fp.write(']')


def main(from_snakemake=True):
    if from_snakemake is False:
        inputs = [sys.argv[i] for i in range(1, len(sys.argv))]
    else:
        inputs = snakemake.input

    analyzed_texts = []
    for input_path in inputs:
        with open(input_path) as fp:
            raw = json.load(fp)
        analyzed_texts.append(TextAnalyzer(raw, include_discarded=True))

    if from_snakemake is False:
        outputs = [sys.stdout for _ in range(0, len(analyzed_texts))]
    else:
        outputs = [open(snakemake.output[path_index], 'w', encoding='utf8') for path_index in
                   range(0, len(snakemake.output))]

    for i_analyzed, analyzed in enumerate(analyzed_texts):
        analyzed.write(outputs[i_analyzed])


try:
    type(snakemake)
    main(True)
except NameError:
    print('[TEST] Executed from outside snakemake')
    main(False)
