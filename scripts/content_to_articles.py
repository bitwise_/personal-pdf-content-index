from bs4 import BeautifulSoup, Tag, NavigableString
import json
import re
import sys


class PartialArticle:

    def __init__(self, page_contents):
        if not isinstance(page_contents, PageContents):
            raise Exception("No 'PageContents' object has been supplied.Supplied {}".format(type(page_contents)))
        self.page_contents = page_contents
        self.text = self.page_contents.get_connected_fragments(PageContents.LONG)
        self.other_fragments = self.page_contents.get_connected_fragments(PageContents.SHORT)

    def dump_as_json(self, fp):
        json.dump({'page': self.page_contents.page_id, 'text': self.text, 'discarded_text': self.other_fragments}, fp,
                  ensure_ascii=False)


class PageContents:
    LONG = 'long'
    SHORT = 'short'

    def __init__(self, xml_page, page_id):

        if not isinstance(xml_page, Tag):
            raise Exception("No 'Tag' object has been supplied.Supplied {}".format(type(xml_page)))

        self.xml_page = xml_page
        self.page_id = page_id
        self.all_fragments = []
        self.long_fragments = []
        self.short_fragments = []

        self.extract_fragments()

    def extract_fragments(self):

        for frg_i, frg in enumerate(self.xml_page.children):
            if isinstance(frg, NavigableString):
                extract_from = frg
                self.add_fragment(TextFragment(frg, 60))
            elif len(frg.contents) == 0:
                print("No 'NavigableString' object has been found in this {} element".format(frg.name))
                print('Skip...')
            elif isinstance(frg.contents[0], NavigableString):
                self.add_fragment(TextFragment(frg.contents[0], 60))
            else:
                print("No 'NavigableString' object has been supplied. Supplied {}".format(type(frg)))
                print('Skip...')

    def add_fragment(self, fragment):

        self.all_fragments.append(fragment)

        if fragment.is_long_text():
            self.long_fragments.append(fragment)
        else:
            self.short_fragments.append(fragment)

    def get_first_long_fragment(self):
        return self.long_fragments[0]

    def get_last_long_fragment(self):
        return self.long_fragments[len(self.long_fragments) - 1]

    def get_connected_fragments(self, which=None):
        if which is None:
            which = self.LONG

        if which is self.LONG:
            this_frgs = self.long_fragments
        elif which is self.SHORT:
            this_frgs = self.short_fragments
        else:
            this_frgs = self.long_fragments

        txt = ''
        for frg in this_frgs:
            txt += ' ' + frg.clean_text
        return txt


class TextFragment:

    def __init__(self, navigable_string, discriminator=0):
        if not isinstance(navigable_string, NavigableString):
            raise Exception("No 'NavigableString' has been supplied.Supplied {}".format(type(navigable_string)))
        self.clean_text = ''
        self.raw_text = '' + navigable_string
        self.cleanup()
        self.discriminator = discriminator

    def get_len(self):
        return len(self.clean_text)

    def cleanup(self):
        self.clean_text = self.raw_text.strip()

        self.clean_text = re.sub(r"(\w+)-\n*(\w+)", lambda m: m.group(1) + m.group(2), self.clean_text)
        self.clean_text = re.sub(r"(\w+)\n+(\w+|\s?)", lambda m: m.group(1) + " " + m.group(2), self.clean_text)
        self.clean_text = re.sub(r"([.,\s”/])\n+", lambda m: m.group(1) + " ", self.clean_text)
        return

    def is_long_text(self):
        return self.get_len() > self.discriminator


def main(from_snakemake=True):
    if from_snakemake is False:
        inputs = [sys.argv[i] for i in range(1, len(sys.argv))]
    else:
        inputs = snakemake.input

    for path_index, raw_text_path in enumerate(inputs):

        with open(raw_text_path) as raw:
            soup = BeautifulSoup(raw, features='html.parser')

        pages = soup.find_all('div', {'class': 'page'})

        partials = []
        for pg_i, pg in enumerate(pages):
            pc = PageContents(pg, pg_i)
            pa = PartialArticle(pc)
            partials.append(pa)

        if from_snakemake is False:
            output = sys.stdout
        else:
            output = open(snakemake.output[path_index], 'w', encoding='utf8')

        with output as out:
            out.write('[')
            for pa_i, pa in enumerate(partials):
                pa.dump_as_json(out)
                if pa_i < len(pages) - 1:
                    out.write(',')
            out.write(']')


try:
    snakemake
    main(True)
except NameError:
    print('[TEST] Executed from outside snakemake')
    main(False)
