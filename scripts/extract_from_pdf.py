#!/usr/bin/env python
from tika import parser
from io import StringIO
from bs4 import BeautifulSoup
import json

xml_output_file_offset = len(snakemake.input)

for input_index, imput_file_path in enumerate(snakemake.input):

    file_data = []
    _buffer = StringIO()
    parsed = parser.from_file(imput_file_path, xmlContent=True)

    xhtml_data = BeautifulSoup(parsed['content'])

    with open(snakemake.output[xml_output_file_offset+input_index], 'w') as output:
        output.write(xhtml_data.prettify())

    for page, content in enumerate(xhtml_data.find_all('div', attrs={'class': 'page'})):
        print('Parsing page {} of {} pdf file...'.format(page+1,''))
        _buffer.write(str(content))
        parsed_content = parser.from_buffer(_buffer.getvalue())
        _buffer.truncate()
        file_data.append({'id': 'page_'+str(page+1), 'content': parsed_content['content']})

    with open(snakemake.output[input_index], 'w') as output:
        json.dump(file_data, output)
